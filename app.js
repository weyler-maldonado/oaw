$(document).ready(function (e) {
    $("#form").on('submit',(function(e) {
     e.preventDefault();
     $.ajax({
            url: "ajaxupload.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
            cache: false,
      processData:false,
      beforeSend : function()
      {
       $("#err").fadeOut();
      }, 
      success: function(data)
         {
       if(data=='invalid')
       {
        $("#err").html("Invalid File!").fadeIn();
       }
         },
        error: function(e) 
         {
       $("#err").html(e).fadeIn();
         }          
       });
    }));
   });

   